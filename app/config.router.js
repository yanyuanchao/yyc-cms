﻿'use strict';
angular.module('app')
    .run(
        [
            '$rootScope', '$state', '$stateParams',
            function($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
        ]
    )
    .config(
        [
            '$stateProvider', '$urlRouterProvider',
            function($stateProvider, $urlRouterProvider) {
                $urlRouterProvider
                    .otherwise('/login');
                $stateProvider
                	.state('login', {
                        url: '/login',
                        templateUrl: 'views/login.html'
                    })
                    .state('logout', {
                        url: '/logout',
                        templateUrl: 'views/logout.html'
                    })
                    .state('app', {
                        abstract: true,
                        url: '/app',
                        templateUrl: 'views/layout.html'
                    })

	                .state('app.article', {
	                    url: '/article/4/{p}',
	                    templateUrl: 'views/article/article-list.shtml',
	                    ncyBreadcrumb: {
	                        label: '文章列表'
	                    },
                        resolve: {
                            deps: [
                                '$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        serie: true,
                                        files: [
                                            './controller/category.js',
                                            './assets/css/dataTables.bootstrap.css'
                                        ]
                                    });
                                }
                            ]
                        }
	                })
	                .state('app.article-add', {
                        url: '/article-add/4/',
                        templateUrl: 'views/article/article-add.shtml',
                        ncyBreadcrumb: {
                        	label: '文章添加'
                        }
                    })
                    .state('app.article-edit', {
                        url: '/article-edit/4/{p}',
                        templateUrl: 'views/article/article-add.shtml',
                        ncyBreadcrumb: {
                            label: '文章编辑'
                        }
                    })
                    .state('app.category', {
                        url: '/category/5/{p}',
                        templateUrl: 'views/article/category/category-list.shtml',
                        ncyBreadcrumb: {
                            label: '栏目列表'
                        }
                    })
                  	.state('app.media', {
                        url: '/media/6/?id',
                        templateUrl: 'views/article/media/media-list.shtml',
                        ncyBreadcrumb: {
                            label: '媒体列表'
                        }
                    })
                  	.state('app.staticize', {
                        url: '/staticize/26/',
                        templateUrl: 'views/article/staticize-list.shtml',
                        ncyBreadcrumb: {
                            label: '静态化管理'
                        }
                    })
                    .state('app.news', {
                        url: '/news/32/{p}',
                        templateUrl: 'views/article/news/news-list.shtml',
                        ncyBreadcrumb: {
                            label: '新闻列表'
                        }
                    })
					.state('app.news-add', {
                        url: '/news-add/32/{id}',
                        templateUrl: 'views/article/news/news-add.shtml',
                        ncyBreadcrumb: {
                            label: '新闻添加'
                        }
                    })
                  	.state('app.poo', {
                        url: '/poo/10/{p}',
                        templateUrl: 'views/healthcenter/poo-list.shtml',
                        ncyBreadcrumb: {
                            label: '便便分析'
                        }
                    })
                    .state('app.pee', {
                        url: '/pee/11/',
                        templateUrl: 'views/healthcenter/pee-list.shtml',
                        ncyBreadcrumb: {
                            label: '尿尿分析'
                        }
                    })
                    .state('app.daily', {
                        url: '/daily/12/',
                        templateUrl: 'views/healthcenter/phr/daily-list.shtml',
                        ncyBreadcrumb: {
                            label: '每日分析'
                        }
                    })
                    .state('app.week', {
                        url: '/week/13/',
                        templateUrl: 'views/healthcenter/phr/week-list.shtml',
                        ncyBreadcrumb: {
                            label: '每周分析'
                        }
                    })
                    .state('app.record', {
                        url: '/record/14/',
                        templateUrl: 'views/healthcenter/phr/record-list.shtml',
                        ncyBreadcrumb: {
                            label: '分析记录'
                        }
                    })
                    .state('app.health_disease', {
                        url: '/healthcenter/disease/7/{p}',
                        templateUrl: 'views/healthcenter/report/disease-list.shtml',
                        ncyBreadcrumb: {
                            label: '疾病管理'
                        }
                    })
                    .state('app.health_situation', {
                        url: '/healthcenter/situation/8/{p}',
                        templateUrl: 'views/healthcenter/report/situation-list.shtml',
                        ncyBreadcrumb: {
                            label: '其他情况'
                        }
                    })
                    .state('app.health_uncoOption', {
                        url: '/healthcenter/uncoOption/9/{p}',
                        templateUrl: 'views/healthcenter/report/uncoOption-list.shtml',
                        ncyBreadcrumb: {
                            label: '选项分析'
                        }
                    })
                    .state('app.health_tip', {
                        url: '/healthcenter/tip/30/{p}',
                        templateUrl: 'views/healthcenter/report/tip-list.shtml',
                        ncyBreadcrumb: {
                            label: '提示内容管理'
                        }
                    })
                    .state('app.autognosis', {
                        url: '/healthcenter/autognosis/29/{p}',
                        templateUrl: 'views/healthcenter/sdl/autognosis-list.shtml',
                        ncyBreadcrumb: {
                            label: '自诊首页'
                        }
                    })
//                  .state('app.autognosis', {
//                      url: '/healthcenter/autognosis/29/{p}',
//                      templateUrl: 'views/healthcenter/sdl/autognosis-list.shtml',
//                      ncyBreadcrumb: {
//                          label: '自诊首页'
//                      },
//                      resolve: {
//                          deps: [
//                              '$ocLazyLoad',
//                              function($ocLazyLoad) {
//                                  return $ocLazyLoad.load(['ui.select']).then(
//                                      function() {
//                                          return $ocLazyLoad.load(
//                                          {
//                                              serie: true,
//                                              files: [
//                                                  'app/controllers/select2.js'
//                                              ]
//                                          });
//                                      }
//                                  );
//                              }
//                          ]
//                      }
//                  })
                    .state('app.disease', {
                        url: '/disease/15/{p}',
                        templateUrl: 'views/disease/disease-list.shtml',
                        ncyBreadcrumb: {
                            label: '疾病列表'
                        }
                    })
                    .state('app.externalitie', {
                        url: '/externalitie/16/{p}',
                        templateUrl: 'views/disease/externalitie-list.shtml',
                        ncyBreadcrumb: {
                            label: '疾病外部因素'
                        }
                    })
                    .state('app.symptom', {
                        url: '/symptom/27/{p}',
                        templateUrl: 'views/disease/symptom-list.shtml',
                        ncyBreadcrumb: {
                            label: '症状列表'
                        }
                    })

	                .state('app.favorite', {
	                    url: '/favorite/18/',
	                    templateUrl: 'views/member/favorite-list.shtml',
	                    ncyBreadcrumb: {
	                        label: '收藏列表'
	                    }
	                })
	                .state('app.member', {
                        url: '/member/17/',
                        templateUrl: 'views/member/member-list.shtml',
                        ncyBreadcrumb: {
                            label: '会员列表'
                        }
	                 })
	                .state('app.consult', {
                        url: '/consult/20/',
                        templateUrl: 'views/consult/consult-list.shtml',
                        ncyBreadcrumb: {
                            label: '我的认领'
                        }
	                 })
	                .state('app.noconsult', {
                        url: '/noconsult/main/19/{p}',
                        templateUrl: 'views/consult/noconsult-list.shtml',
                        ncyBreadcrumb: {
                            label: '未认领'
                        }
                    })
                    .state('app.consult-main', {
                        url: '/consult/main/{p}',
                        templateUrl: 'views/consult/consult-main.shtml',
                        ncyBreadcrumb: {
                            label: '咨询消息'
                        }
                    })
                    
                    .state('app.account', {
	                    url: '/account/21/{p}',
	                    templateUrl: 'views/auth/account-list.shtml',
	                    ncyBreadcrumb: {
	                        label: '用户管理'
	                    }
                	})
                    .state('app.role', {
                        url: '/role/22/{p}',
                        templateUrl: 'views/auth/role-list.shtml',
                        ncyBreadcrumb: {
                            label: '角色管理'
                        }
                    })
                    .state('app.menu', {
	                    url: '/menu/23/{p}',
	                    templateUrl: 'views/auth/menu-list.shtml',
	                    ncyBreadcrumb: {
	                        label: '菜单管理'
	                    }
                	})
                    .state('app.appgrade', {
                        url: '/appgrade/24/{p}',
                        templateUrl: 'views/app/appgrade-list.shtml',
                        ncyBreadcrumb: {
                            label: '升级管理'
                        }
                    })
                 	.state('app.feedback', {
                        url: '/feedback/25/{p}',
                        templateUrl: 'views/app/feedback-list.shtml',
                        ncyBreadcrumb: {
                            label: '反馈列表'
                        }
                    })
                    .state('app.role_permission', {
                        url: '/rolepermission/{id}',
                        templateUrl: '/views/auth/rolepermission.shtml',
                        ncyBreadcrumb: {
                            label: '角色分配权限'
                        }
                    })
                    .state('app.flashpage', {
                        url: '/flashpage/31/{p}',
                        templateUrl: 'views/app/flashpage-list.shtml',
                        ncyBreadcrumb: {
                            label: '闪屏页管理'
                        }
                    })
                    .state('app.banner', {
                        url: '/banner/35/{p}',
                        templateUrl: 'views/app/banner-list.shtml',
                        ncyBreadcrumb: {
                            label: 'Banner管理'
                        }
                    }).state('app.app_json', {
                        url: '/app_json/39/',
                        templateUrl: 'views/app/jsonManage/json.shtml',
                        ncyBreadcrumb: {
                            label: 'Json文件管理'
                        }
                    })
                    .state('app.taskQueue', {
                        url: '/taskQueue/33/{p}',
                        templateUrl: 'views/timer/taskQueue-list.shtml',
                        ncyBreadcrumb: {
                            label: '点对点任务'
                        }
                    })
                    .state('app.taskTopic', {
                        url: '/taskTopic/34/{p}',
                        templateUrl: 'views/timer/taskTopic-list.shtml',
                        ncyBreadcrumb: {
                            label: '订阅任务'
                        }
                    })
                    .state('app.dictionary', {
                        url: '/dictionary/36/{p}',
                        templateUrl: 'views/dictionary/banner/list.shtml',
                        ncyBreadcrumb: {
                            label: '字典数据管理'
                        }
                    }) .state('app.ostsFragment', {
                        url: '/fragment/37/',
                        templateUrl: 'views/osts/fragment/fragment_list.shtml',
                        ncyBreadcrumb: {
                            label: '碎片管理'
                        }
                    })
                    .state('app.ostsTemplate', {
                    url: '/template/38/',
                    templateUrl: 'views/osts/template/template_list.shtml',
                    ncyBreadcrumb: {
                        label: '模板管理'
                    }
                });

            }
        ]
    );


