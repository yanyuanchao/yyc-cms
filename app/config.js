var app =
    angular.module('app')
        .config(
        [
            '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function($controllerProvider, $compileProvider, $filterProvider, $provide) {
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
            }
        ]);


app.config(function($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
        template: '<ul class="breadcrumb"><li><i class="fa fa-home"></i><a>首页</a></li><li ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li></ul>'
    });
});



$.format = function (source, params) {
    if (arguments.length == 1)
        return function () {
            var args = $.makeArray(arguments);
            args.unshift(source);
            return $.format.apply(this, args);
        };
    if (arguments.length > 2 && params.constructor != Array) {
        params = $.makeArray(arguments).slice(1);
    }
    if (params.constructor != Array) {
        params = [params];
    }
    $.each(params, function (i, n) {
        source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
    });
    return source;
};

var jsonMenu='./json/menu.json';
var jsonPhr='./json/phr.json';

var getLoginMenu='/server/menus';
var getLoginPemissions='/server/pemissions/{0}';
var getLoginServerUrl='/server/login';

var categoryServerUrl='/ams/category';
var getCategoryListServerUrl=categoryServerUrl+'/list';
var getArticleCategoryServerUrl=categoryServerUrl+'/all';
var delArticleCategoryServerUrl='/ams/articles/delArticleCategory?articleId={0}&categoryId={1}';
var addArticleCategoryServerUrl='/ams/articles/addArticleCategory?articleId={0}&categoryId={1}';

var articleServerUrl='/ams/articles';
var newsServerUrl='/ams/news';
var getArticleServerUrl=articleServerUrl+'/list';
var getArticleByIdServerUrl=articleServerUrl+'/{0}';
var offArticleServerUrl= articleServerUrl+'/off/{0}';
var onArticleServerUrl=articleServerUrl+ '/on/{0}';
var mediaServerUrl='/ams/media';
var getAllMediaServerUrl=mediaServerUrl+'/all';
var getMediaServerUrl=mediaServerUrl+'/list';
var getNewsServerUrl=newsServerUrl+'/list';
var getNewsByIdServerUrl=newsServerUrl+'/{0}';

var getPhrDiseaseServerUrl='/phr/handler/disease';
var getPhrDiseaseAllServerUrl='/phr/handler/disease/all';
var getPhrSituationServerUrl='/phr/handler/situation';
var getPhrSituationAllServerUrl='/phr/handler/situation/all';
var getPhrSuggestionServerUrl='/phr/handler/suggestion';
var getPhrSuggestionUncoOptionServerUrl='/phr/handler/suggestion/queryByUncoOption?key={0}';
var getPhrUncoOptionServerUrl='/phr/handler/uncoOption';


var appGradeServerUrl='/app/grade';
var getAppGradeServerUrl='/app/grade/query';
var appFeedbackServerUrl='/app/feedback';
var getAppFeedbackServerUrl=appFeedbackServerUrl+'/query';
var handleAppFeedbackServerUrl=appFeedbackServerUrl+'/handle/{0}';

var authAccountServerUrl='/auth/account';
var getAuthAccountServerUrl=authAccountServerUrl+'/list';
var authMenuServerUrl='/auth/menu';
var getAuthMenuServerUrl=authMenuServerUrl+'/list';
var authRoleServerUrl='/auth/role';
var getAuthRoleServerUrl=authRoleServerUrl+'/list';

