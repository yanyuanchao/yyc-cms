function ajax(obj) {
	obj.type = obj.type == null ? 'POST' : obj.type;
	obj.async=obj.async==null?true:obj.async;
	obj.data = obj.data == null ? {} : obj.data;
	obj.timeout = obj.timeout == null ? 10000 : obj.timeout;
	obj.beforeSend = obj.beforeSend == null ? function( xhr ) {$("#loading").show();xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); } : obj.beforeSend;
	obj.success = obj.success == null ? function(){} : obj.success;
	obj.error = obj.error == null ?function(jqXHR, textStatus, errorThrown){"服务器出错了"} : obj.error;
	obj.complete = obj.complete == null ? function(){setTimeout('$("#loading").hide()',500);} : obj.complete;
	obj.contentType = obj.contentType == null ? 'application/x-www-form-urlencoded; charset=UTF-8' : obj.contentType;
	$.ajax({
		type: obj.type,
		async:obj.async,
		timeout:  obj.timeout,
		url: baseURI + obj.url,
		contentType: obj.contentType,
		beforeSend: obj.beforeSend,
		data: obj.data,
		success: obj.success,
		complete : obj.complete,
		error: obj.error
	});
}

/*获取参数*/
var hrefstr,pos;
hrefstr = window.location.href;
pos = hrefstr.indexOf("?");
function getQueryString(name) {
	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if(r!=null)return  unescape(r[2]); return null;
}



var lServerUrl;
var p=1;
var lListName="";
var lShowDel;
var lShowAdd;
var lDiseaseType="";


//查询
//var title="";
var searchName="";
var searchGenders="";
var searchValue="";
var searchMode="";


//回车查询
enter("sobtn");


function changeMode() {
	p=1;
	searchMode=$("#searchMode").val();
	common_getTempData(lServerUrl,lShowDel,lShowAdd,lListName);
}



/*
异步执行
serverUrl:获取数据接口地址
type:ajax传递类型（get,post,put,delete）
formId:表单id（需要提交数据的表单id）
local:数据所在位置（本地:true,远程:false)
 */
function common_ajax(serverUrl,type,formId,callback) {
	ajax({
		type: type==""?'get':type,
		url:serverUrl,
		async:true,
		data:formId==""?{}:$("#"+formId).serialize(),
		success: function(resp) {
			if(typeof(resp)!='object'&&resp!=null)
				resp=eval('('+resp+')');
			if(resp)
			{
				if(resp.status==401) {
					if(location.hash!='#/login')
						location.href = '/#/login';
					if(type!='get')
						alert(resp.message);
					return ;
				}
				else if(resp.status==403)
					return ;
			}
			callback(resp);
		}

	});
}
//同步执行
function common_ajaxTb(serverUrl,type,formId) {
	var dataList=null;
	ajax({
		type: type==""?'get':type,
		url:serverUrl,
		async:false,
		data:formId==""?{}:$("#"+formId).serialize(),
		success: function(resp) {
			if(typeof(resp)!='object'&&resp!=null)
				resp=eval('('+resp+')');
			if(resp)
			{
				if(resp.status==401) {
					if(location.hash!='#/login')
						location.href = '/#/login';
					if(type!='get')
						alert(resp.message);
					return ;
				}
				else if(resp.status==403)
				{
					return ;
				}
			}
			dataList=resp;
		}

	});
	return dataList;
}


function  common_ajaxLocal(serverUrl,type) {
	var dataList=null;
	$.ajax({
		type:type==undefined? "get":type,
		async:false,
		dataType:"json",
		cache:false,
		url: serverUrl,
		contentType:"Content-type: text/html; charset=utf8",
		success: function(resp) {
			if(typeof(resp)!='object')
				resp=eval('('+resp+')');
			dataList=resp;
		}
	});
	return dataList;
}


/*文章预览*/
function common_show(obj,gourl){
	gourl=gourl+'?url='+$(obj).attr("url");
	layer.open({
		type: 2,
		title: false,
		shadeClose: true,
		area: [($(window).width()/4-30)+'px',(($(window).width()/4-30)*777/400)+'px'],
		skin: 'layui-layer-nobg',
		maxmin: false,
		closeBtn: 1,
		content:gourl
	});

}


/*获取不同列表数据*/
function common_getTempData(serverUrl,showDel,showAdd,listName,isPaging) {
	if(isPaging==undefined)  //是否需要分页
	    isPaging=true;

	if(lServerUrl!=serverUrl) {
		p=1;
		lServerUrl = serverUrl;
		title=$("#keyword").val();
		searchName=$("#searchName").val();
		searchValue=$("#searchValue").val();
		searchGenders=$("#searchGenders").val();
		categoryid=$("#category").val();
	}
	else
	{
		$("#keyword").val(title);
		$("#searchName").val(searchName);
		$("#searchValue").val(searchValue);
		$("#searchGenders").val(searchGenders);
		$("#category").val(categoryid);
	}
	if(p>1)
	{
		var rs=location.hash.substring(location.hash.lastIndexOf("."));
		if(!isNaN(rs))
		{
			var pall=parseFloat(location.hash.substring(location.hash.lastIndexOf('/')+1));
			p=pall-parseFloat(rs);
		}
	}


	lListName=listName;
	lShowAdd=showAdd;
	lShowDel=showDel;

	if(listName=='疾病外部因素')
		lDiseaseType=2;
	else if(listName=='疾病列表')
	   lDiseaseType=1;

	if(!showDel)
	   $("#del_model").hide();
	$("#listName").html(listName);
	if(!showAdd)
		$("#addUrl").hide();
//	else
//		$("#addName").html("添加"+listName);
	var pageSize=10;
	if(isPaging) {
		//设置列表显示条数session，需要引入jquerysession.js
		if ($.session.get('pagesize') == undefined || $.session.get('pagesize') == 'undefined') {
			$.session.set('pagesize',10);
			pageSize =10;
		}
		else {
			pageSize = $.session.get('pagesize');
			$("#pageSize").val(pageSize);
		}
	}
	else
	{
		$("#pageSize").hide();
		pageSize=100;
		p=1;
	}
	common_delMore(serverUrl.replace("query","").replace("list",""),p);

	if(categoryid>0)
		serverUrl=serverUrl.replace("/list","")+'/category/'+categoryid+"?size="+pageSize;
	else
		serverUrl=serverUrl+"?size="+pageSize+"&type="+lDiseaseType+(title==""||title==undefined?"":"&title="+title)+(searchName==""||searchName==undefined?"":"&name="+searchName)+(searchValue==""||searchValue==undefined?"":"&value="+searchValue)+(searchGenders==""||searchGenders==undefined?"":"&genders="+searchGenders)+(searchMode==""||searchMode==undefined?"":"&mode="+searchMode);

	var resp=common_ajaxTb(serverUrl+"&p="+p+"&current="+p+"&page="+p,'get','');
	if(resp&&resp.status==200) {
		createHtml(resp);
		if(isPaging) {
			if (resp.paging.count <=1)
				$("#page").hide();
			else
				$("#page").show();
			//分页初始化
			laypage({
				cont: 'page',
				pages: resp.paging.count,
				skip: true, //是否开启跳页
				 skin: '#03b3b2',
				 groups: 5,//连续显示分页数
				 curr: resp.paging.current,
				jump: function (e, first) { //触发分页后的回调
				if (!first) { //一定要加此判断，否则初始时会无限刷新
				p = e.curr;
				currentPage(serverUrl + '&p=' + p+"&current="+p+"&page="+p);
				}
				}
			});
		}
	}else
	{
		alert(resp.message);
	}
}



//当前分页数据获取
function currentPage(url){
	ajax({
		type:'GET',
		url: url,
		success: function(resp) {
			if(resp.result) {
				//清空数据区
				$("#list_box").empty();
				createHtml(resp);
			}
		}
	});
}

//绑定数据到页面
function  createHtml(dataList) {
	var tpl = document.getElementById('tpl').innerHTML;
	if(typeof(dataList)!='object')
		dataList=eval('(' + dataList.replace(/[\r\n]/g,'')+ ')');
	var  html=template(tpl, dataList);
	document.getElementById('list_box').innerHTML = html;

}

/*提交编辑*/
function common_submit(serverUrl,action,formId) {
	var resultList;
	ajax({
		type:action==null?"post":action,
		url: serverUrl,
		async:false,
		data: $("#"+formId).serialize(),
		success: function(resp) {
			resultList=resp;
		}
	});
	return resultList;

}
function  common_commitDefault(paraStr,serverUrl,formId,hrefUrl) {
	ajax({
		type:paraStr>0?"put": "post",
		url: serverUrl,
		data: $("#"+formId).serialize(),
		success: function(resp) {
			if(resp.status == 500) {
				layer.msg(resp.message,{icon:2,time: 2000});
				return;
			}
			else{
				layer.msg('保存成功',{time: 1000});
				setTimeout(function(){
					location.href =hrefUrl+(p+Math.random());
				}, 900);
			}
		}
	});
}


/*删除*/
function common_del(obj,url){
	layer.confirm('确认要删除吗？' ,function(){
		var result=common_ajax(url,"delete","");
		if(result.status==200) {
			layer.msg('已删除!', {icon: 1, time: 1000});
			$(obj).parents("tr").remove();
		}
		else
		{
			layer.msg('删除失败!', {icon: 1, time: 1000});
		}
	});
}

/*批量删除*/
function common_delMore(serverUrl,currPage) {
	// 全选
	$("#allChk").click(function() {
		$("input[name='subChk']").prop("checked",this.checked);
	});
	// 单选
	var subChk = $("input[name='subChk']")
	subChk.click(function() {
		$("#allChk").prop("checked", subChk.length == subChk.filter(":checked").length ? true:false);
	});

	$("#del_model").click(function() {
		var checkedNum = $("input[name='subChk']:checked").length;
		if(checkedNum == 0) {
			layer.msg('至少选择一项!',{icon:0,time:1000});
			return;
		}
		layer.confirm('确认要删除吗？' ,function(index){
			var checkedList = new Array();
			$("input[name='subChk']:checked").each(function() {
				checkedList.push($(this).val());
			});
			ajax({
				type: "delete",
				url: serverUrl+'/'+checkedList,
				success: function(result) {
					if(result.status==200) {
						layer.msg('已删除!', {icon: 1, time: 1000});
						setTimeout(function () {
							var hash=location.hash.substring(0,location.hash.lastIndexOf('/'));
							location.href=location.pathname.toLowerCase()+hash+'/'+(currPage+Math.random());
						}, 900);
					}
					else
					{
						layer.msg('删除失败!', {icon: 1, time: 1000});
					}
				}
			});
		});
	});
}

/*编辑小窗口*/
var editHtml='';
function editInfo(id,currPage){
	var formId="defaultForm";
	if(editHtml!=''&&$("#editBox").html()=='') {
		$("#editBox").html(editHtml);
	}
	var dataList;
	var b=true;
	var hidenType=$("#type").val();

	if(id!="") {
		dataList =common_ajax(lServerUrl.replace("/query","").replace("/list","")+'/'+id,'get','').result;
		for (var key in dataList) {
			var selectId = document.getElementById(key);
			if ((selectId != null&&selectId.nodeName == "SELECT")||typeof(dataList[key])=="object") {
				editAdd(dataList[key],selectId,key);
			}
			else if ((selectId != null&&selectId.nodeName == "TEXTAREA")) {
				$('#' + key).text(dataList[key]);
			}
			else
			{
				$('#' + key).attr("value", dataList[key]);
			}
		}
	}
	else {
		editAdd(null,"","");
		$("#defaultForm :input").attr("value", "").removeAttr("checked").remove("selected");
		$ ("textarea").text('');
		$("#type").val(hidenType);
		$("#device").removeAttr("disabled");
		$("#channel").removeAttr("disabled");
		$("#version").removeAttr("readonly");
	}

	editHtml=$("#editBox").html();
	$("#editBox").html("");
	bootbox.dialog({
		message:editHtml,
		title:id!=''?"编辑":"新增",
		className: "modal-darkorange",
		animate: true,
		buttons: {
			Cancel: {
				label: "取消",
				className: "btn-blue",
				callback: function () { }
			},
			Success: {
				label:"保存",
				className: "btn-danger",
				callback: function () {
					b=true;
					$("#"+formId).find("input").each(function() {
						var id = $(this).attr("id");
						var text=$("#"+id).prev().html();
						if($("#"+id).val()==""&&$(this).attr("rel")=="必填")
						{
							layer.msg(text+"不能为空",{icon:2,time: 1000});
							$("#"+id).focus();
							b=false;
						}
					});
					if(b) {
						$("#device").removeAttr("disabled");
						$("#channel").removeAttr("disabled");
						$("#uncoOptionId").removeAttr("disabled");
						ajax({
							type: id!=""? "put" : "post",
							url: lServerUrl.replace("/query","").replace("/list",""),
							contentType:'application/x-www-form-urlencoded; charset=UTF-8',
							data: $("#"+formId).serialize(),
							beforeSend: function () {
								// 禁用按钮防止重复提交
								$(".modal-darkorange").attr({ disabled: "disabled" });
							},
							success: function (resp) {
								if (resp.status == 500) {
									layer.msg(resp.message, {icon: 2, time: 2000});
									return false;
								}
								else {
									layer.msg('保存成功', {time: 1000});
									if(id!="") {
										setTimeout(function () {
											var hash = location.hash.substring(0, location.hash.lastIndexOf('/'));
											location.href = location.pathname.toLowerCase() + hash + '/' + (currPage + Math.random());
										}, 900);
									}
								}
							},
							complete: function () {
								$(".modal-darkorange").removeAttr("disabled");
							}
						});
					}
					else
						return false;
				}
			}
		}
	});
}

//编辑
function editAdd(dataList,selectId,key) {
}
function subInfo(){

}

//解析json数组（递归）
function   analyzeJson(data) {
	var dataStr="";
	if(typeof(data)=="object"&&data!=null)
	{
		if(data.length>0) {
			for (var i = 0; i < data.length; i++) {
				for (var key in data[i]) {
					if(key!="id"&&key!="updateTime"&&key!="position") {
						dataStr += analyzeJson(data[i][key]);
						if(i<data.length-1)
							dataStr+=",";
					}

				}
			}
		}
		else
		{
			for (var key in data) {
				if(key!="id"&&key!="updateTime"&&key!="position") {
					dataStr += data[key];
				}
			}
		}
	}
	if(dataStr=="")
		dataStr=data==null?"":data;
	return dataStr;
}

function ctrlAndEnter(btnId) {
	$(document).keydown(function(e) {
		if (e.ctrlKey && e.which == 13)
			$("#"+btnId).click();
	})
}


function enter(btnId) {
	$(document).keydown(function(e) {
		e = e || window.event;
		if (e.which == 13) {
			$("#" + btnId).click();
		}
	})
}

/*当前日期*/
function todayTime() {
	var mydate=new Date();
	var y=mydate.getFullYear();
	var m=mydate.getMonth()+1<10?"0"+(mydate.getMonth()+1):(mydate.getMonth()+1);
	var d=mydate.getDate()<10?"0"+mydate.getDate():mydate.getDate();
	var h=mydate.getHours()<10?"0"+mydate.getHours():mydate.getHours();
	var hm=mydate.getMinutes()<10?"0"+mydate.getMinutes():mydate.getMinutes();
	var date=y+'-'+m+'-'+d+' '+h+':'+hm;

	return date;
}

/*右侧弹窗*/
function showAlert(content)
{
	Notify(content,'bottom-right', '2000', 'danger', '', true);

}

function Notify(message, position, timeout, theme, icon, closable) {
	toastr.options.positionClass = 'toast-' + position;
	toastr.options.extendedTimeOut = 0; //1000;
	toastr.options.timeOut = timeout;
	toastr.options.closeButton = closable;
	toastr.options.iconClass = icon + ' toast-' + theme;
	toastr['custom'](message);
}


//修复select2单选在model弹窗无法搜索问题
$.fn.modal.Constructor.prototype.enforceFocus = function () { };









