
app.controller("login",function($scope,$location,$stateParams) {

    $scope.accountName='1234567';
    $scope.headImg='./assets/img/avatars/logo_128.jpg';
    $scope.role='管理员';

    /*登录*/
    $scope.login=function() {
        var $account=$("#account");
        var $passWord=$("#passWord");
       $("#rememberMe").val(document.getElementById("remember").checked);
        if($account.val()!=''&&$passWord.val()!='') {
            $scope.saveUserInfo("post","loginForm",0);
        }
        else
        {
            if($account.val()=='') {
                $("#groupPass").removeClass("login-error");
                $("#groupAccount").addClass("login-error");
                $account.attr("placeholder","请输入账号");
                $account.focus();
            }
            else if($passWord.val()=='') {
                $passWord.attr("placeholder","请输入密码");
                $("#groupAccount").removeClass("login-error");
                $("#groupPass").addClass("login-error");
                $passWord.focus();
            }
        }
    }

    /*退出*/
    $scope.logout=function() {
        common_ajax("/server/logout","get","",function(){
            location.href="./";
        });

    }

    /*全部权限*/
    $scope.permission='';
    $scope.nowPermission=function(){
        $scope.url=$location.path();
        if($scope.url!="")
         {
             $scope.arr=$scope.url.split('/');
             $scope.menuid=$scope.arr[$scope.arr.length-2];
             if(!isNaN($scope.menuid))
             {
              common_ajax("/server/pemissions/"+$scope.menuid,'get','',function(permissionData){
                  if(permissionData&&permissionData.status==200&&permissionData.result.length>0)
                  {
                      $scope.permission=permissionData.result+',';
                  }
                  else
                  {
                      $scope.permission='';
                      alert(permissionData.message);
                  }
               });
             }
             else {
                 $scope.permission='';
                 alert("对不起，您没有权限！");
             }
         }
    }

    /*打开页面，判断是否有页面操作权限*/
    $scope.isPermission= function (sign) {
        if($scope.permission.indexOf(sign)>=0)
            return true;
        return false;
    }

    /*保存信息*/
    $scope.saveUserInfo=function(type,formId,state) {
        common_ajax(getLoginServerUrl,type,formId,function(resp){
            if(resp&&resp.status==200) {
                if (resp.result.length > 0) {
                    $scope.accountName='1234';
                    $scope.headImg='./assets/img/avatars/logo_128.jpg';
                    $scope.role='管理员';
                    if(state<2)
                        window.location.href = "./#/app/article/4/";
                }
                else
                    alert("对不起，您没有足够的权限进入！");
            }
        });

    }
})
