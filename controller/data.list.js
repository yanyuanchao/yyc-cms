app.service("dataService", function () {

    this.normData=[{"type":0,"name":"其他栏位"},{"type":1,"name":"特殊"},{"type":2,"name":"其他不适"},{"type":3,"name":"颜色"},{"type":4,"name":"形状"},{"type":5,"name":"感觉"},{"type":6,"name":"气味"},{"type":7,"name":"造口"},{"type":8,"name":"形状颜色"},{"type":9,"name":"排量"}];
    this.typeData=[{"type":1,"name":"尿尿"},{"type":2,"name":"便便"}];
    this.levelData=[{"type":0,"name":"无级"},{"type":1,"name":"一级"},{"type":2,"name":"二级"},{"type":3,"name":"三级"}];


    this.getNorm=function(norm) {
        for(var i=0;i<this.normData.length;i++)
            if(this.normData[i].type==norm) {
                return this.normData[i].name;
                break;
            }
    }

    this.getType=function(type) {
        for(var i=0;i<this.typeData.length;i++)
            if(this.typeData[i].type==type) {
                return this.typeData[i].name;
                break;
            }
    }

    this.getLevel=function(level) {
        for(var i=0;i<this.levelData.length;i++)
            if(this.levelData[i].type==level) {
                return this.levelData[i].name;
                break;
            }
    }
})

app.controller("dataList",function($scope,dataService) {

    $scope.baseUrl=baseURI;
    $scope.fileUrl=fileURI;
    $scope.allNormData=dataService.normData;
    $scope.allTypeData=dataService.typeData;
    $scope.allLevelData=dataService.levelData;
    $scope.datas=null; //列表数据
    $scope.pageSize=10;
    $scope.serverUrl="";
    $scope.pageServerUrl="";
    $scope.categoryid=0;
    $scope.p=1;
    $scope.title="";
    $scope.singlechk=false; //选中状态初始化

    $scope.so=true;//是否显示搜索

    /*点击表格头排序*/
    $scope.sortList=function(type){
        $("table").find('th').each(function(){
            if($(this).attr("id"))
            {
                $(this).removeClass();
                $(this).addClass("sorting");
            }
        })

        $scope.orderByName=type;
        $scope.orderByStyle=$("#"+type).attr("value");
        $("#"+type).removeClass();
        $("#"+type).addClass("sorting_"+$scope.orderByStyle);
        if($scope.orderByStyle=='asc')
            $("#"+type).attr("value","desc");
        else
            $("#"+type).attr("value","asc");
        $scope.loadData();
    }




    /*搜索*/
    $scope.soList=function() {
        $scope.tagsKeyword=$("#tagsKeyword").val();
        $scope.searchValue=$("#keyword").val();
        $scope.searchName=$("#keyword").attr("name");
        $scope.p=1;
        $scope.loadData();
    }

    /*搜索标签*/
    $scope.soTags=function()
    {
        $scope.tagsKeyword=$("#tagsKeyword").val();
        $scope.searchValue=$("#keyword").val();
        $scope.p=1;
        $scope.loadData();

    }

    /*选择栏目*/
    $scope.changeCategory=function() {
        $scope.categoryid=$("#category").val();
        $scope.p=1;
        $scope.loadData();
    }

    /*选择性别*/
    $scope.changeGenders=function () {
       $scope.p=1;
     $scope.searchGenders=$("#searchGenders").val();
        $scope.loadData();
    }

    /*选择性别*/
    $scope.changeFeedbackType=function () {
        $scope.p=1;
        $scope.feedbackType=$("#type").val();
        $scope.loadData();
    }

    /*选择条数*/
    $scope.changePageSize=function() {
        $.session.set('pagesize',$("#pageSize").val());
        $scope.p=1;
        $scope.pageSize=$("#pageSize").val();
        $scope.loadData();

    }

    $scope.loadData=function(){
        $scope.dataList($scope.serverUrl,$scope.isPaging,$scope.listName,$scope.so);
    }

    /*加载数据*/
    $scope.dataList=function(serverUrl,isPaging,listName,search) {
        if($scope.pageServerUrl!=serverUrl)
        {
            $scope.pageServerUrl=serverUrl;
            $scope.p=1;
            $scope.searchValue='';
            $scope.searchGenders='';
            $scope.categoryid=0;
            $scope.feedbackType='';
            $scope.orderByName='';
            $scope.orderByStyle='';
            $("#category").val("");
        }
        else {
            $("#keyword").val($scope.searchValue);
        }

        if($("#allChk").val()!=undefined) {
            document.getElementById("allChk").checked = false;
            $scope.allCheck(false);
            $scope.singlechk = false;
        }
        $scope.isPaging=isPaging;
        $scope.listName=listName;
        $scope.serverUrl = serverUrl;
        $scope.so=search==undefined?false:search;
        $scope.getPageSize();

        var contact='';
        if(listName=="疾病列表")
          contact='&type=1';
        else if(listName=='疾病外部因素')
            contact='&type=2';

        var genders=''
        if($scope.searchGenders!=''&&$scope.searchGenders!=undefined)
            genders='&genders='+$scope.searchGenders;

        var feedbackType='';
        if($scope.feedbackType!='')
            feedbackType = '&type=' + $scope.feedbackType;

        var orderBy='';
        if($scope.orderByName!=''&&$scope.orderByStyle!='')
            orderBy = '&orderByName=' + $scope.orderByName+"&orderByStyle="+$scope.orderByStyle;

        var category='';
        if(($scope.categoryid>0||$scope.categoryid!="")&&$scope.categoryid!=null)
            category='&categoryIds='+$scope.categoryid;

        var tags="";
        if($scope.tagsKeyword!=undefined&&$scope.tagsKeyword!='')
            tags='&tags='+$scope.tagsKeyword;

            serverUrl=$scope.serverUrl+"?size="+$scope.pageSize+($scope.searchValue==""||$scope.searchValue==undefined?"&name=":"&"+$scope.searchName+"="+$scope.searchValue)+"&p="+$scope.p+"&current="+$scope.p+"&page="+$scope.p+contact+genders+feedbackType+orderBy+category+tags;

        $scope.loadPage(serverUrl);
    }

    $scope.loadPage=function(serverUrl) {
       common_ajax(serverUrl,'get', "",function(data){
           if (data) {
            if(data.status == 200)
                $scope.datas=data.result;
            else {
                showAlert(data.message);
                $scope.datas = null;
            }
        }else {
               showAlert(data.message);
               $scope.datas = null;
           }
        $scope.page(data);
        if(!$scope.$$phase) {
            $scope.$apply();
        }       });
    }

    /*获取显示条数*/
    $scope.getPageSize= function () {
        if($scope.isPaging) {
            if ($.session.get('pagesize') == undefined || $.session.get('pagesize') == 'undefined') {
                $.session.set('pagesize',10);
                $scope.pageSize =10;
            }
            else {
                $scope.pageSize = $.session.get('pagesize');
                $("#pageSize").val($scope.pageSize);
            }
        }
        else
        {
            $("#pageSize").hide();
            $scope.pageSize=100;
            $scope.p=1;
        }
    }

    /*分页*/
    $scope.page=function(resp){
        if($scope.isPaging&&resp!=null) {
            if (resp.paging.count <=1)
                $("#page").hide();
            else
                $("#page").show();
            laypage({
                cont: 'page',
                pages: resp.paging.count,
                skip: true, //是否开启跳页
                skin: '#03b3b2',
                groups: 5,//连续显示分页数
                curr: resp.paging.current,
                jump: function (e, first) { //触发分页后的回调
                    if (!first) { //一定要加此判断，否则初始时会无限刷新
                        $scope.p=e.curr;
                        $scope.loadData();
                    }
                }
            });
        }
    }

    /*多选*/
    $scope.choseArr=[];//定义数组用于存放前端显示
    $scope.allCheck=function(allChk) {
        $scope.choseArr=[];
        $scope.singlechk=allChk;
        if(allChk)
        {
            for(var i=0;i<$scope.datas.length;i++)
                $scope.choseArr.push($scope.datas[i].id);
        }
    }

    /*单选*/
    $scope.singleCheck=function(singleChk,id) {
        if(singleChk)
        {
            if( $scope.choseArr.indexOf(id)==-1)
                $scope.choseArr.push(id) ;
        }
        else {
            if( $scope.choseArr.indexOf(id)!=-1) {
                var index = $scope.choseArr.indexOf(id);
                $scope.choseArr.splice(index, 1);
            }
        }
    }

   /*单个删除*/
    $scope.delete=function(id) {
        layer.confirm('确认要删除吗？' ,function(){
           common_ajax($scope.serverUrl.replace("/query","").replace("/list","").replace("/search","").replace("/index","")+'/'+id,'delete','',function(result){
               if(result.status==200) {
                   layer.msg('已删除!', {icon: 1, time: 1000});
                   $scope.$apply(function(){
                       for(var i=0; i<$scope.datas.length;i++){
                           if($scope.datas[i].id==id){
                               $scope.datas.splice(i,1);
                           }
                       }
                   })
               }
               else
               {
                   layer.msg('删除失败!', {icon: 1, time: 1000});
               }
           });

        });
    }

    /*批量删除*/
    $scope.deleteMore=function() {
        if($scope.choseArr.length == 0) {
            layer.msg('至少选择一项!',{icon:0,time:1000});
            return;
        }
        layer.confirm('确认要删除吗？' ,function(){
            var checkedList=$scope.choseArr;
           common_ajax($scope.serverUrl.replace('/list','').replace("/query","").replace("/search","").replace("/index","")+'/'+checkedList,'delete','', function (result){
                if(result.status==200) {
                    layer.msg('已删除!', {icon: 1, time: 1000});
                    $scope.loadData();
                }
                else
                {
                    layer.msg('删除失败!', {icon: 1, time: 1000});
                }
            });

        });
    }

    /*选项清空缓存*/
    $scope.wipeCache=function(){
        var result=common_ajax("/phr/handler/uncoOption/cache","delete","",function(){
            if(result.status==200) {
                layer.msg('清除成功!', {icon: 1, time: 1000});
            }
            else
            {
                layer.msg('清除失败!', {icon: 2, time: 1000});
            }
        });

    }


    $scope.edit=function(id) {
        $('.select2-container').remove();
        $("#picturs").html("");
        $("#fileList").html("");
        $scope.formData={"id":id};
        if(id!=""&&id!="0")
        {
            $(".modal-title").html("编辑");
          var data=common_ajaxTb($scope.serverUrl.replace("/query","").replace("/list","").replace("/search","").replace("/index","")+'/'+id,'get','');
            if(data!=null&&data.status==200)
            {
                $scope.formData=data.result;


                if($("#modeList").length>0) {
                    $("#modeList").val($scope.formData.modeList);
                    $scope.modeList=$scope.formData.modeList;
                }
                if($("#uncoDiseaseIds").length>0) {

                    var arr=[];
                    if($scope.formData.uncoOptionDiseases.length>0) {
                        for (var h = 0; h < $scope.formData.uncoOptionDiseases.length; h++) {
                            arr.push($scope.formData.uncoOptionDiseases[h].disease.id.toString());
                        }
                    }
                    $scope.diseaseIds=arr;
                    $("#uncoDiseaseIds").val($scope.diseaseIds);
                }

                if($("#symDiseaseIds").length>0)
                {
                    var arrDisease=[];
                    if($scope.formData.diseases.length>0)
                    {
                        for (var i=0;i<$scope.formData.diseases.length;i++) {
                            arrDisease.push($scope.formData.diseases[i].id.toString());
                        }
                    }

                    $("#symDiseaseIds").val(arrDisease);
                    $scope.symDiseaseIds=arrDisease;
                }

                if($("#situationIds").length>0) {
                    var arr=[];
                    if($scope.formData.uncoOptionSituations.length>0) {
                        for (var h = 0; h < $scope.formData.uncoOptionSituations.length; h++) {
                            arr.push($scope.formData.uncoOptionSituations[h].situation.id.toString());
                        }
                    }
                    $scope.situationIds=arr;
                    $("#situationIds").val($scope.situationIds);

                }


                if($("#roleIds").length>0) {
                    var arr=[];
                    if($scope.formData.roles)
                    {
                        for(var i=0;i<$scope.formData.roles.length;i++)
                            arr.push($scope.formData.roles[i].id.toString());
                    }
                    $("#roleIds").val(arr);
                    $scope.roleIds=eval(arr);

                }

                if($("#bannerChannel").length>0) {
                    var arr=[];
                    if($scope.formData.channel!="")
                    {
                        for(var i=0;i<$scope.formData.channel.split(',').length;i++)
                            arr.push($scope.formData.channel.split(',')[i]);
                    }
                    $("#bannerChannel").val(arr);
                    $scope.channel=eval(arr);
                }



            }
        }
        else
        {
            $('input').removeAttr("readonly");
            $('select').removeAttr("disabled");
            if($("#modeList").length>0) {
                $("#modeList").val("");
                $scope.modeList="";
            }
            if($("#uncoDiseaseIds").length>0) {
                $("#uncoDiseaseIds").val("");
                $scope.diseaseIds="";
            }
            if($("#symDiseaseIds").length>0) {
                $("#symDiseaseIds").val("");
                $scope.diseaseIds="";
                $("#followSymptomIds").val("");
                $("#tags").val("");
            }
            if($("#situationIds").length>0) {
                $("#situationIds").val("");
                $scope.situationIds="";
            }
            if($("#roleIds").length>0) {
                $("#roleIds").val("");
                $scope.roleIds="";
            }

            if($("#bannerChannel").length>0) {
                $("#bannerChannel").val("");
                $scope.channel="";
            }
        }
        $(".multiSelect").select2({placeholder: "请选择",allowClear: true});
    }

    /*modal弹出提交*/
    $scope.editOther=function(id,formId) {
        $('input').removeAttr("readonly");
        $('select').removeAttr("disabled");
        subInfo();
        id=$("#"+id).val();
        var action="post";
        if(id!=""&&id!="0")
          action='put';
        common_ajax($scope.serverUrl.replace("/query","").replace("/list","").replace("/search","").replace("/index",""),action,formId,function(submitData){
            if(submitData&&submitData.status==200) {
                $('#editModal').modal('hide');
                layer.msg('保存成功',{time: 1000});
                $scope.loadData();
            }
            else
                layer.msg('保存失败',{time: 1000});
        });


    }

    /*获取*/
    $scope.getNorm=function(norm) {
        return dataService.getNorm(norm);
    }

    /*获取记录类型*/
    $scope.getType=function(type) {
        return dataService.getType(type);
    }

    /*获取等级*/
    $scope.getLevel=function(level) {
        return dataService.getLevel(level);
    }

    /*模式字典*/
    $scope.getModel=function() {
        $scope.modelData=$scope.dicDataFunction("user_model");
    }

    /*性别字典*/
    $scope.getGenders=function() {
        $scope.gendersData= $scope.dicDataFunction("genders");
    }

    /*设备字典*/
    $scope.getDevice=function() {
        $scope.deviceData=$scope.dicDataFunction("device");

    }

    /*渠道字典*/
    $scope.getChannel=function() {
        $scope.channelData=$scope.dicDataFunction("channel");
    }

    /*banner点击类型字典*/
    $scope.getBannerClickType=function() {
        $scope.bannerClickTypeData=$scope.dicDataFunction("app_banner_clickType");
    }

    /*症状记录类型*/
    $scope.getSymptomRecordType=function() {
        $scope.symptomRecordTypeData=$scope.dicDataFunction("disease_symptom_recordType");
    }

    /*phr类型*/
    $scope.getPhrType=function() {
        $scope.phrTypeData=$scope.dicDataFunction("phr_type");
    }

    /*phr提示*/
    $scope.getPhrTipScene=function() {
        $scope.phrTipSceneData=$scope.dicDataFunction("phr_tip_scene");
    }

    /*模板碎片管理-状态*/
    $scope.ostsStstus=function() {
        $scope.ostsStatusData=$scope.dicDataFunction("osts_status");
    }

    /*碎片管理-类型*/
    $scope.ostsFragmentType=function() {
        $scope.ostsFragmentTypeData=$scope.dicDataFunction("osts_fragment_type");
    }

    //字典数据管理
    $scope.dicDataFunction=function(key) {
        $scope.dicData=null;
        $scope.dicData=common_ajaxTb("/app/dictionary/select?key="+key,"get","");
        if($scope.dicData) {
            return $scope.dicData.result;
        }
        else
            return null;
    }

})